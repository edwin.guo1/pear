import Vuex from 'vuex'
import firebase from 'firebase'

const Store = () => {
  return new Vuex.Store({
    state: {
        user: null,
        error: null,
        loading: null,
        signInMethod: null,
        uid: null,
        uni: null,
        id: null,
        name: null,
        profile_picture: null,
        uni: null,
        tut: null,
        description: null,
        disableSwipe: null,
        /*
        loadedProfile: 
            {
                id: null,
                name: null,
                profile_picture: null,
                uni: null,
                tut: null,
                description: null
            }, */
    	profileCourses: {
			courses: {}
        },
        match: {
            profile: {}
        },
		matchVal: null, 
		chat: null, 
		chat_ID: null, 
    },
    mutations: {
        setUser (state, payload) {
            state.user = payload
        },
        setUni (state, payload) {
            state.uni = payload
        },
        setError (state, payload) {
            state.error = payload
        },
        setLoading (state, payload) {
            state.loading = payload
        },
        setGoogleMethod (state, payload) {
            state.signInMethod = payload
        },
        setFbMethod (state, payload) {
            state.signInMethod = payload
        },
        setLoadedProfile (state, payload) {
            //state.loadedProfile = payload

            state.id = payload.id
            state.name = payload.name
            state.profile_picture = payload.profile_picture
            state.uni = payload.uni 
            state.tut = payload.tut
            state.description = payload.description
        },
        setUid (state, payload) {
            state.uid = payload
        },
        updateProfile (state, payload) {
            if (payload.tut) {
              state.tut = payload.tut
            }
            if (payload.description) {
              state.description = payload.description
            }
            if (payload.uni) {
              state.uni = payload.uni
            }
        },
    	setCourses (state, payload) {
        	state.profileCourses.courses = payload
        },
        updateCourse (state, payload) {
            //state.profileCourses.courses[payload.course] = 'true'   
            state.profileCourses.courses = {...state.profileCourses.courses,[payload.course]: 'true'}           
        },
        delCourse (state, payload) {
            delete state.profileCourses.courses[payload.course]
            state.profileCourses.courses = {...state.profileCourses.courses}
        },
        match (state, payload) {
            //state.match = payload
            state.match.profile = payload
            state.match.profile = {...state.match.profile}
        },
		matchVal (state, payload) {
			state.matchVal = payload
		},
		chat (state, payload) {
			state.chat = payload
			state.chat = {...state.chat}
		},
		chat_ID (state, payload) {
			state.chat_ID = payload
        },
        disableSwipe (state, payload) {
            state.disableSwipe = payload
        }
    },
    actions: {
        signIn ({commit}, payload) {
            commit('setLoading', true)

            if (payload.provider === 'fb'){
                var provider = new firebase.auth.FacebookAuthProvider()
            }
            if (payload.provider === 'google'){
                var provider = new firebase.auth.GoogleAuthProvider()
            }
                
            
            firebase.auth().signInWithPopup(provider)
            .then(function(result) {
                var user = result.user
                var name, email, photoUrl, uid, emailVerified, avatar;

                name = user.displayName;
                email = user.email;
                photoUrl = user.photoURL;
                emailVerified = user.emailVerified;
                uid = user.uid;
                commit('setUid', uid)

                console.log(name)
                console.log(photoUrl)
                console.log(uid)

                user.providerData.forEach(function (profile) {
                    console.log("Sign-in provider: " + profile.providerId)
                    console.log("  Provider-specific UID: " + profile.uid)
                    console.log("  Name: " + profile.displayName)
                    console.log("  Email: " + profile.email)
                    commit('setUser', profile.displayName)
                    if (payload.provider === 'fb') {
                        console.log("  Photo URL: " + "https://graph.facebook.com/" + profile.uid + "/picture?height=500")
                        avatar = 'https://graph.facebook.com/' + profile.uid + '/picture?height=500'
                    }
                    else {
                        console.log("  Photo URL: " + profile.photoURL)
                        avatar = profile.photoURL
                    }
                  });

                firebase.database().ref('users/' + uid).once('value').then(function(snapshot) {
                    if (snapshot.val() == null) {
                    //create profile details in firebase
                        firebase.database().ref('users/' + uid).set({
                            name: name,
                            profile_picture : avatar,
                            uni: "USYD",
                            tut: "tutee",
                            description: "hello I just signed up",
                            id: uid
                          })
                        console.log('profile created')
                        firebase.database().ref('users/' + uid).once('value')
                        .then((data) => {
                            console.log(data.val())
                        })
                    }
                    else {
                        console.log('no profile created')
                    }
                
                        firebase.database().ref('users/' + uid).once('value')
                        .then((data) => {
                            console.log(data.val())
                            console.log(data.val().courses)

                            const obj = data.val()
                            var profile = {
                                id: obj.id,
                                name: obj.name,
                                profile_picture: obj.profile_picture,
                                uni: obj.uni,
                                tut: obj.tut,
                                description: obj.description,
                            	//courses: obj.courses
                              }
                            var courses = obj.courses
                                                    
                              commit('setLoadedProfile', profile)
                        	  commit('setCourses', courses)
							  
							  firebase.database().ref('chats/' + uid).once('value')
							  .then((data) => {
								  commit('chat', data.val())
							  })
                        })
                
                  });

              commit('setLoading', null)
              commit('setError', null)

            })
            .catch(error => {
              alert(error.message)
              commit('setError', error.message)
              commit('setLoading', null)
            })
          },
          googleMethod ({commit}) {
            commit('setGoogleMethod', 'google')
          },
          fbMethod ({commit}) {
            commit('setFbMethod', 'fb')
          },
          signout ({commit}) {
            firebase.auth().signOut()
            commit('setUser', null)
          },
          updateProfile ({commit}, payload) {
            var uid = payload.uid
            commit('setLoading', true)
            console.log(payload)
            const updateObj = {}
            if (payload.uni) {
              updateObj.uni = payload.uni
              
            }
            if (payload.description) {
              updateObj.description = payload.description
              
            }
            if (payload.tut) {
              updateObj.tut = payload.tut
              
            }
            firebase.database().ref('users/' + uid).update(updateObj)
              .then(() => {
                commit('setLoading', null)
                commit('updateProfile', payload)
                commit('setUni', payload.uni)
                UIkit.modal('#modal-uni').hide()
                UIkit.modal('#modal-description').hide()
                UIkit.modal('#modal-tut').hide()
              })
              .catch(error => {
                console.log(error)
                commit('setLoading', null)
              })
          },
    	  updateCourse ({commit}, payload) {
          	  var uid = payload.uid
              var courseObj = {[payload.course]: "true"}
              var courseReg = {[payload.uid]: "true"}
              //console.log(courseObj)
              console.log(payload)
          	firebase.database().ref('users/' + uid + '/courses').update(courseObj)
            .then(() => {
                //UIkit.modal('#modal-course').hide()  
                firebase.database().ref('courses/' + payload.course).push(payload.uid)
                .then(() => {
                //commit('updateCourse', payload)
                commit('updateCourse', payload)
                UIkit.modal('#modal-course').hide()
                })
            })

          },
          delCourse ({commit}, payload) {
              var uid = payload.uid
              var courseObj = {[payload.course]: null}
              var courseReg = {[payload.uid]: null}

              //console.log(payload)
            firebase.database().ref('users/' + uid + '/courses').update(courseObj)
            .then(() => {
                firebase.database().ref('courses/' + payload.course).orderByValue().equalTo(payload.uid).once("value")
                .then((data) => {
                    var obj = data.val()
                    var key = Object.keys(obj)
                    var courseDel = {[key]: null}
                    firebase.database().ref('match/' + payload.uid + '/' + payload.course).remove()
                    firebase.database().ref('courses/' + payload.course).orderByValue().equalTo(payload.uid).once("value")
                    .then((data) => {
                        let objCourse = data.val()
                        let key = Object.keys(objCourse)
                        let delObj = {[key]: null}
                        firebase.database().ref('courses/' + payload.course).update(delObj)
                        .then(() => {
                            commit('delCourse', payload)
                        })
                    })
                })
            })
          },
          getMatch ({commit}, payload) {
			  commit('setLoading', true)
            commit('disableSwipe', null)  
            //check if initialised match
            firebase.database().ref('match/' + payload.uid + '/' + payload.course).once("value")          
            .then((data) => {    
				//match does not exist
                if (data.val() == null) {
                    //get the first key 
                    firebase.database().ref('courses/' + payload.course).limitToFirst(1).once("value")
                    .then((data) => {
                      let obj = data.val()
                      let key = Object.keys(obj)
					  let keyval = key[0]
                      let lastMatch = {[keyval]: 'true'}
                      console.log(lastMatch)
					  //set match
                      firebase.database().ref('match/' + payload.uid + '/' + payload.course).update(lastMatch)
                    })
                }
			})
			.then(() => {
				firebase.database().ref('match/' + payload.uid + '/' + payload.course).once("value")
				.then((data) => {
				  let obj = data.val()
				  //console.log(obj)
                  let key = Object.keys(obj)
                  let val = Object.values(obj)
                  let valval = val[0]
                  var keyval = key[0]
                  if (valval == 'false') {
					  //check if new users exists
                    firebase.database().ref('courses/' + payload.course).orderByKey().startAt(keyval).limitToFirst(2).once("value")
                    .then((data) => {
					  let obj = data.val()
                      let key = Object.keys(obj)
                      var nextkey = key[1]
					  //no new users
                      if (nextkey == null) {
                        //console.log('no next key')
                        commit('disableSwipe', true)
                        commit('match', {
                            profile_picture: null,
                            name: 'exhausted all matches, please try again later',
                            description: null,
                            tut: null,
                            })
                      }	
					  //there are new users
				      else {
							let newMatch = {[nextkey]: 'true'}
							//set new value
							firebase.database().ref('match/' + payload.uid + '/' + payload.course).set(newMatch)
							.then((data) => {
								  firebase.database().ref('match/' + payload.uid + '/' + payload.course).once("value")
								  .then((data) => {
									let obj = data.val()
									//console.log(obj)
									let val = Object.values(obj)
									var keyval = key[0]
										firebase.database().ref('courses/' + payload.course).orderByKey().equalTo(keyval).once("value")
										.then((data) => {
										let matchObj = data.val()
										//console.log(matchObj)
										let matchValue = Object.values(matchObj)
										let matchVal = matchValue[0] 
										//console.log(matchVal)

										//get match data
										firebase.database().ref('users/' + matchVal).once("value")
										  .then((data) => {
										  //console.log(data.val())
										  commit('match', data.val())
										}) 
									  })
								  })
							})
						}
                    })
                  }
                  else {
				  commit('matchVal', keyval)
				  //get match id 
				  firebase.database().ref('courses/' + payload.course).orderByKey().equalTo(keyval).once("value")
					.then((data) => {
					let matchObj = data.val()
					//console.log(matchObj)
					let matchValue = Object.values(matchObj)
					let matchVal = matchValue[0] 
					//console.log(matchVal)

					//get match data
					firebase.database().ref('users/' + matchVal).once("value")
					  .then((data) => {
					  //console.log(data.val())
					  commit('match', data.val())
					}) 
                  })
                }
				})
			})
			commit('setLoading', null)
        },
        updateMatch ({commit}, payload) {
			commit('setLoading', true)
            commit('disableSwipe', null)
			//get current match
			firebase.database().ref('match/' + payload.uid + '/' + payload.course).once("value")
			.then((data) => {
				var curMatch = data.val()
				//console.log(obj)
				var curKey = Object.keys(curMatch)
                var curMatch = curKey[0]			
				
					//get current id of match 
					firebase.database().ref('courses/' + payload.course).orderByKey().startAt(curMatch).limitToFirst(2).once("value")
					.then((data) => {
						let curID = data.val()
						let curVals = Object.values(curID)
						let curKeys = Object.keys(curID)
						var accept = curVals[0]
						var prevKeys = curKeys[0]
                        let acceptObj = {[accept]: 'true'}
                        
                        //add verdict
						if (payload.verdict == 'yes') {
							firebase.database().ref('accepted/' + payload.uid + '/' + payload.course).update(acceptObj)
                        }
                        
                        //check if other user matched 
                        firebase.database().ref('accepted/' + accept + '/' + payload.course).orderByKey().equalTo(payload.uid).once("value")
                        .then((data) => {
                          console.log(data.val())
                          if (data.val() != null) {
                            //console.log('there is a couple')                            
                                let couple = {}
                                couple.id = payload.uid
                                couple.course = payload.course
                                couple.name = payload.name
                            
                                let chatroom = {
                                [payload.uid]: 'true',
                                [accept]: 'true',
                                course: payload.course
                                }
                                
                            //console.log(chatroom)
                            
                            //create chatroom
                            firebase.database().ref('chatroom/').push(chatroom)
                            .then((data) => {
                                var chatID = (data.key)
                                //console.log(chatID)
								firebase.database().ref('users/' + payload.uid).once('value')
								.then((data) => {
									let objuser = data.val()
									var firstmatch = {
									chat: chatID,
									name: objuser.name,
									course: payload.course,
									url: objuser.profile_picture
									}
								firebase.database().ref('chats/' + accept).push(firstmatch)
								})
                                
                                firebase.database().ref('users/' + accept).once('value')
                                .then((data) => {
                                  let obj = data.val()
								  var secmatch = {
									chat: chatID,
									name: obj.name,
									course: payload.course,
									url: obj.profile_picture
								  }
                                firebase.database().ref('chats/' + payload.uid).push(secmatch)
                                })
								
								//update your matches
								firebase.database().ref('chats/' + payload.uid).once('value')
								.then((data) => {
								  commit('chat', data.val())
								})
							  
                            })
                            }
                        })
						
						if (curKeys[1] == null) {
                        console.log('end of the line') 
                        let newMatch = {[prevKeys]: 'false'}
                        firebase.database().ref('match/' + payload.uid + '/' + payload.course).set(newMatch)
						.then((data) => {
                            commit('match', {
                                profile_picture: null,
                                name: 'exhausted all matches, please try again later',
                                description: null,
                                tut: null,
                                })
                            commit('disableSwipe', true)
                        })
						}
						
						else {
							//set new match
							var nextKeys = curKeys[1]
							let newMatch = {[nextKeys]: 'true'}
							var newID = curVals[1]
							//set new value
							firebase.database().ref('match/' + payload.uid + '/' + payload.course).set(newMatch)
							.then((data) => {
								//get new id
								firebase.database().ref('users/' + newID).once('value')
								.then((data) => {
									//console.log(data.val())
									commit('match', data.val())
								}) 
							})
						}
						
					})
			})
				commit('setLoading', null)
        },
		chat_ID ({commit}, payload) {
			commit('chat_ID', payload)
		},
		sendChat ({commit}, payload) {
			console.log(payload)
			firebase.database().ref('chatroom/' + payload.chat_ID + '/log').push(payload)
        },
        updateMatches ({commit}, payload) {
			commit('setLoading', true)
            firebase.database().ref('chats/' + payload).once('value')
            .then((data) => {
                commit('chat', data.val())
				commit('setLoading', null)
            })
        },

    },
    getters: {
        isAuthenticated (state) {
          return state.user !== null && state.user !== undefined
        },
        loadedProfile: state => {
            const obj = {}
            
            obj.id = state.id,
            obj.name = state.name,
            obj.profile_picture = state.profile_picture,
            obj.uni = state.uni,
            obj.tut = state.tut,
            obj.description = state.description

            return obj 
        },
        loadedCourses: state => {
            const obj = {}
            obj.courses = state.profileCourses.courses
            return obj.courses
            
        },
        loadedMatch: state => {
            var obj = {}
            obj.match = state.match.profile
            return obj.match
        },
        loadedChats: state => {
            var obj = {}
            obj = state.chat
            return obj
        },
		chat_ID: state => {
			return state.chat_ID
		}
    },

  })
}

export default Store
